# -*- coding: utf-8 -*-
from decimal import Decimal, ROUND_HALF_UP


def _check(*args):
    """
    检查所有参数是不是字符串型
    :param args: 多个参数
    """
    for arg in args:
        if not isinstance(arg, str):
            raise Exception('参数必须是一个数字型字符串')


def plus(digit_left, digit_right):
    """
    两个Decimal数相加
    :param digit_left: 左侧 Decimal数值
    :param digit_right: 右侧 Decimal数值
    :return:
    """
    _check(digit_left, digit_right)

    r = Decimal(digit_left) + Decimal(digit_right)
    return str(r)


def minus(digit_left, digit_right):
    """
    两个Decimal数相减
    :param digit_left: 左侧 Decimal数值
    :param digit_right: 右侧 Decimal数值
    :return:
    """
    _check(digit_left, digit_right)

    r = Decimal(digit_left) - Decimal(digit_right)
    return str(r)


def multiply(digit_left, digit_right):
    """
    两个Decimal数相乘
    :param digit_left: 左侧 Decimal数值
    :param digit_right: 右侧 Decimal数值
    :return:
    """
    _check(digit_left, digit_right)

    r = Decimal(digit_left) * Decimal(digit_right)
    return str(r)


def divide(digit_left, digit_right):
    """
    两个Decimal数相除
    :param digit_left: 左侧 Decimal数值
    :param digit_right: 右侧 Decimal数值
    :return:
    """
    _check(digit_left, digit_right)

    r = Decimal(digit_left) / Decimal(digit_right)
    return str(r)


def quantize(digit, precision):
    """
    正常的四舍五入（非奇进偶舍）
    :param digit: Decimal数值
    :param precision: 四舍五入精度
    :return:
    """
    _check(digit, precision)

    r = Decimal(digit).quantize(Decimal(precision), ROUND_HALF_UP)
    return str(r)


def compare(digit_left, digit_right):
    """
    比较左右两个Decimal数值
    左侧大则返回1，右侧大则返回-1，一样大返回0
    :param digit_left: 左侧 Decimal数值
    :param digit_right: 右侧 Decimal数值
    :return:
    """
    _check(digit_left, digit_right)

    r = Decimal(digit_left).compare(Decimal(digit_right))
    return str(r)


def formatter(digit, pattern):
    """
    格式化数字
    :param digit: Decimal数值
    :param pattern: 格式化格式
    :return:
    """
    _check(digit, pattern)

    r = pattern.format(Decimal(digit))
    return r


if __name__ == '__main__':
    print('MeMath - More Efficient UiBot Extension')

    # x = '14.4125'
    # y = '1.3337'
    #
    # rplus = plus(x, y)
    # print('plus:', rplus)
    #
    # rminus = minus(x, y)
    # print('minus:', rminus)
    #
    # rmultiply = multiply(x, y)
    # print('multiply:', rmultiply)
    #
    # rdivide = divide(x, y)
    # print('divide:', rdivide)
    #
    # rquantize = quantize(rdivide, '0.0000')
    # print('quantize', rquantize)
    #
    # rcompare = compare(rplus, y)
    # print('compare:', rcompare)
    #
    # rformatter = formatter(rdivide, "{:.4f}")
    # print('formatter:', rformatter)
