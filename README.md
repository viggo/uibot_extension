<h1 align="center"><img src="./logo.png" alt="">
<div>More Efficient UiBot Extension</div>
</h1>
<p align="center">UiBot 扩展集/自定义命令集，提供多种实用功能，旨在进一步提高工作效率！</p>

---

## 扩展命令集

- **MeCache** 全局内存缓存
    - put 设置缓存（可设置缓存过期时间）
    - get 获取缓存（可设置默认值）
    - delete 删除缓存
    - clear 清空所有缓存
    - has 判断缓存是否存在和有效（未过期）
    - popitem 弹出缓存（弹出后删除该条目）
    - keys 获取所有缓存键名 
    - values 获取所有缓存键值
    - items 获取所有缓存条目
    - size 获取缓存条目个数
    
- **MeCaptcha** 第三方验证码识别接口集成（图鉴，超级鹰）
    - tujian 图鉴验证码识别接口（AI识别）
    - tujian_report_error 图鉴识别报错接口
    - chaojiying 超级鹰验证码识别接口（打码平台识别）
    - chaojiying_report_error 超级鹰识别报错接口
    
- **MeImage** 图像处理（基于 Pillow）
    - load 加载本地/远程图片
    - save 保存图片到本地
    - size 获取图片大小
    - resize 更改图片大小
    - convert 更改图片色彩模式
    - crop 裁剪图片
    - to_clip 将图片保存到剪贴板
    - screen_capture 全屏截图
    - to_base64 将图片转换为base64字符串
    - from_base64 将base54字符串转换为图片
    - show 调用本地默认图片查看器查看图片
    
- **MeMath** 精确计算（基于 Decimal）
    - plus 加法
    - minus 减法
    - multiply 乘法
    - divide 除法
    - quantize 四舍五入（非奇进偶舍，可设定保留小数位）
    - compare 比较大小
    - formatter 格式化数字

## 使用方式

**一、作为 Python 插件使用**

1. 将目录内的`.py`文件复制到UiBot安装根目录内的`/extend/python` 目录中
2. 在流程代码块内`Import 文件名`

```
// 使用示例
import MeCache

MeCache.set('MeCache', 'More Efficient UiBot Extension')
```

**二、安装共享命令**

1. 启动`UiBot Creator`
2. 打开`UiBot命令中心 -> 共享命令 -> 开发者共享`
3. 找到`ME`图标的命令集并安装

**三、安装自定义命令**
1. 将扩展命令集目录压缩成`.zip`压缩包
2. 启动`UiBot Creator`
3. 打开`UiBot命令中心 -> 自定义命令 -> 我的命令 -> 新增命令模块`
4. 根据提示安装

## 开源协议

[GPL-3.0](https://gitee.com/viggo/uibot_extension/blob/master/LICENSE)

文档进一步完善中...